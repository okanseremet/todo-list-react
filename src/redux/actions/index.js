import { DELETE_TODO, GET_TODOS, SUBMIT_TODO, TOGGLE_TODO } from '../constants';
import { addTodoDoReq, getTodoListDoReq, removeTodoDoReq, toggleTodoDoReq } from '../../services/todo.service';
import { mockApi } from '../../mockApi';

export const addTodo = (text) => {
    mockApi.mockAddTodo(text);
    return async (dispatch) => {
        const response = await addTodoDoReq({ text, isChecked: false });
        console.log('response', response)
        dispatch({
            type: SUBMIT_TODO,
            payload: response.todos
        });
    }
}


export const getTodoList = () => {
    mockApi.mockGetTodos();
    return async (dispatch) => {
        const payload = await getTodoListDoReq();
        console.log('payload', payload)
        dispatch({
            type: GET_TODOS,
            payload
        });
    };
};


export const removeTodo = (id) => {
    mockApi.mockDeleteTodo(id);
    return async (dispatch) => {
        try {
            const response = await removeTodoDoReq(id);
            console.log('response', response)

            dispatch({
                type: DELETE_TODO,
                id
            });
        } catch (err) {
            console.error(err);
        }
    };
};


export const toggleTodo = (id) => {
    mockApi.mockToggleTodo(id);
    return async (dispatch) => {
        try {
            const response = await toggleTodoDoReq(id);
            console.log('response', response)

            dispatch({
                type: TOGGLE_TODO,
                id
            });
        } catch (err) {
            console.error(err);
        }
    };
};

