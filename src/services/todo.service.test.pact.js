/**
 * @jest-environment node
 */

import { Pact } from '@pact-foundation/pact';
import Todo from '../model/todo';
import Return from '../model/return';
import { addTodoDoReq, getTodoListDoReq, removeTodoDoReq, toggleTodoDoReq } from './todo.service';
import { somethingLike } from '@pact-foundation/pact/dsl/matchers';

const path = require('path');

global.fetch = require("node-fetch");

const provider = new Pact({
    consumer: 'consumer',
    provider: 'provider',
    port: 8080,
    log: path.resolve(`${process.cwd()}/contract`, 'logs', 'pact.log'),
    logLevel: 'warn',
    dir: path.resolve(`${process.cwd()}/contract`, 'pacts'),
    spec: 2
});
describe('API Pact test', () => {


    beforeAll(() => provider.setup());
    afterEach(() => provider.verify());
    afterAll(() => provider.finalize());

    describe('getting all todos', () => {
        test('todos exists', async () => {

            // set up Pact interactions
            await provider.addInteraction({
                state: 'todo exists',
                uponReceiving: 'get all todos',
                withRequest: {
                    method: 'GET',
                    path: '/api/todos',

                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: []
                },
            });


            // make request to Pact mock server
            const product = await getTodoListDoReq();

            expect(product).toStrictEqual([

            ]);
        });
    })
    describe('add todo api', () => {
        test('add Batsin bu dunya as a todo', async () => {
            let todoTobeAdded = new Todo(1, 'Batsin bu dunya', false);
            let expectedResponse = new Return('todo created and added.', todoTobeAdded)
            // set up Pact interactions
            await provider.addInteraction({
                state: 'hello world todo object',
                uponReceiving: 'add hello world as a todo',
                withRequest: {
                    method: 'POST',
                    path: '/api/todos',
                    body: todoTobeAdded

                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: somethingLike(expectedResponse),
                },
            });


            // make request to Pact mock server
            const product = await addTodoDoReq(todoTobeAdded);

            expect(product).toEqual(new Return('todo created and added.', todoTobeAdded));
        });
    })

    describe('toggle todo api', () => {
        test('toggle todo as completed', async () => {
            const todoId = 1
            let todoTobeChecked = new Todo(1, 'Batsin bu dunya', true);
            let expectedResponse = new Return('todo checked successfully.', todoTobeChecked)
            // set up Pact interactions
            await provider.addInteraction({
                state: 'hello world todo',
                uponReceiving: 'will be checked',
                withRequest: {
                    method: 'PUT',
                    path: `/api/todos/${todoId}`,

                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: somethingLike(expectedResponse),
                },
            });


            // make request to Pact mock server
            const product = await toggleTodoDoReq(todoId);

            expect(product).toEqual(new Return('todo checked successfully.', todoTobeChecked));
        });
    })

    describe('delete todo api', () => {
        test('delete a todo', async () => {
            const todoId = 1
            let expectedResponse = new Return('todo was deleted', null)
            // set up Pact interactions
            await provider.addInteraction({
                state: 'hello world todo',
                uponReceiving: 'will be destroyed ',
                withRequest: {
                    method: 'DELETE',
                    path: `/api/todos/${todoId}`,

                },
                willRespondWith: {
                    status: 200,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    body: somethingLike(expectedResponse),
                },
            });


            // make request to Pact mock server
            const product = await removeTodoDoReq(todoId);

            expect(product).toEqual(new Return('todo was deleted', null));
        });
    })

})
