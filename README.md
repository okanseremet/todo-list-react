# Todo React-App Assignment
 - This is a simple to-do app. You can create, delete todos and mark as completed.
- Backend of the Assignment was coded with Golang. I developed FE side with React JS. I used CRA to speed up the process and implemented redux for state management. While making http requests with `axios` I have encountered cors issues so I had to use `fetch`.


## Status of Assignment
I think the project is almost complete. You can see a demonstration of the app [here](http://167.71.254.102:3001/). There are some work to do but it can provide main functionalities in the given acceptance criteria.

## Components
 UI side consists of 3 components. These are:
- todo
- todolist
- addtodo

## Testing
This project was developed using A-TDD and CDC approach. 


### CDC - Consumer Driven Contracts
I used `pact` tool to write contract tests. Pact.io has a tool called `pact-flow` publishing and verifying pacts between provider and consumer. I preferred to use this tool instead of the `pact-broker` docker solution. Because I did't have enough time dockerizing and implementing it on cloud side. I think docker solution would be better for CI/CD side. It gives us the full control of CDC process without any dependencies. I need to make some practice.
### Unit Tests
I wrote unit tests using Enzyme. It does not work properly with CRA react version so I had to install `@wojtekmaj/enzyme-adapter-react-17` package.

### e2e Tests
I used nightmare.js for automation tests. I couldn't use mock server of pact.io for e2e and implemented `fetch-mock`. 

## CI/CD
I decided to use Gitlab pipeline for this project. Gitlab provides a lot of functionality in one place so it seems to be a good solution for developers. There is no need a lot of configuration. It has it's own docker registry, codebase and environment variables etc. We need only a `gitlab-ci.yml`, `Dockerfile`, `nginx.conf`. All we have to do is configure this files and push our code to relevant branch.   

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
  

### `npm test:ci`

Launches the test runner for pipeline.
 I had to add `--testPathIgnorePatterns=src/e2e/e2e.test.js` line to prevent test errors on CI side. Because e2e tests need X session to run. 

### `npm test:pact`

Launches the consumer contract tests and publishes the pact to pact-flow tool.
  


### Missing Steps of Assignment
- e2e tests not working on cloud for now. I should have used puppeteer for easy and rapid integration. It seems to be well-documented to integrate headless approach. I will give it a shot later.

